-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 16 Décembre 2015 à 17:01
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `sitekaggle`
--

-- --------------------------------------------------------

--
-- Structure de la table `compte_rendu`
--

CREATE TABLE IF NOT EXISTS `compte_rendu` (
  `idCR` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `idReunion_FOREIGN` int(11) NOT NULL,
  PRIMARY KEY (`idCR`),
  KEY `idReunion_FOREIGN` (`idReunion_FOREIGN`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `compte_rendu`
--

INSERT INTO `compte_rendu` (`idCR`, `url`, `titre`, `description`, `idReunion_FOREIGN`) VALUES
(8, 'user_cr/lol@lol/compte_rendu_de_lol@lol_n_8.pdf', '', '', 1),
(9, 'user_cr/lol@lol/compte_rendu_de_lol@lol_n_9.pdf', '', '', 2),
(10, 'user_cr/paul@passat/compte_rendu_de_paul@passat_n_10.pdf', '', '', 1),
(11, 'user_cr/paul@passat/compte_rendu_de_paul@passat_n_11.pdf', '', '', 1),
(12, 'user_cr/paul@passat/compte_rendu_de_paul@passat_n_12.pdf', '', '', 1),
(13, 'user_cr/paul@passat/compte_rendu_de_paul@passat_n_13.pdf', '', '', 1),
(14, 'user_cr/paul@passat/compte_rendu_de_paul@passat_n_14.pdf', '', '', 1),
(15, 'user_cr/paul@passat/compte_rendu_de_paul@passat_n_15.pdf', '', '', 1),
(16, 'user_cr/paul@passat/compte_rendu_de_paul@passat_n_16.pdf', '', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `demande_ajout`
--

CREATE TABLE IF NOT EXISTS `demande_ajout` (
  `idDemande` int(11) NOT NULL AUTO_INCREMENT,
  `idPers` int(11) NOT NULL,
  `idEquipe_FOREIGN` int(11) NOT NULL,
  PRIMARY KEY (`idDemande`),
  KEY `idEquipe_FOREIGN` (`idEquipe_FOREIGN`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

CREATE TABLE IF NOT EXISTS `equipe` (
  `idEquipe` int(11) NOT NULL AUTO_INCREMENT,
  `nomEquipe` varchar(32) NOT NULL,
  `idChefEquipe` int(11) NOT NULL,
  PRIMARY KEY (`idEquipe`),
  KEY `idEquipe` (`idEquipe`),
  KEY `idEquipe_2` (`idEquipe`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `equipe`
--

INSERT INTO `equipe` (`idEquipe`, `nomEquipe`, `idChefEquipe`) VALUES
(3, 'dream_team', 7),
(4, 'the bobos', 8);

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE IF NOT EXISTS `personne` (
  `idPers` int(11) NOT NULL AUTO_INCREMENT,
  `prenomPers` varchar(32) NOT NULL,
  `nomPers` varchar(32) NOT NULL,
  `mailPers` varchar(32) NOT NULL,
  `statutPers` int(11) NOT NULL,
  `passwordPers` varchar(255) NOT NULL,
  `idEquipe_FOREIGN` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPers`),
  KEY `idEquipe_FOREIGN` (`idEquipe_FOREIGN`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `personne`
--

INSERT INTO `personne` (`idPers`, `prenomPers`, `nomPers`, `mailPers`, `statutPers`, `passwordPers`, `idEquipe_FOREIGN`) VALUES
(5, 'passat', 'paul', 'paul@passat', 2, '$2y$10$2huEz.Nu8yOOxDMWpYLdWuRtDmb2mrLwh6NhDj0l6TbknhK8T7Mda', NULL),
(11, 'dauphin', 'baptiste', 'lol@lol', 3, '$2y$10$A124b03Z2jbhgG5TKQN6FuA3PkXMGng61S7Wx8fYIQGrUVF29LsPK', NULL),
(13, 'bonjour', 'benoit', 'equipier', 2, '$2y$10$uRKsTbb7MWfqFVTgRF8w1.XP38x5srfnC1sRPsnNeZswMHYLAqFfS', NULL),
(14, 'Oursel', 'Alexandre', 'escro', 1, '$2y$10$3cQZSw7suxjmyZPU6nWB0Ob5QFBZcm8tLKqg.ARp6z.sDZqQPqXbq', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `reunion`
--

CREATE TABLE IF NOT EXISTS `reunion` (
  `idReunion` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) NOT NULL,
  `date` date NOT NULL,
  `ordre` varchar(64) DEFAULT NULL,
  `idEquipe_FOREIGN` int(11) NOT NULL,
  PRIMARY KEY (`idReunion`),
  KEY `idEquipe_FOREIGN` (`idEquipe_FOREIGN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `personne`
--
ALTER TABLE `personne`
  ADD CONSTRAINT `personne_ibfk_1` FOREIGN KEY (`idEquipe_FOREIGN`) REFERENCES `equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reunion`
--
ALTER TABLE `reunion`
  ADD CONSTRAINT `idEquipe_Foreign` FOREIGN KEY (`idEquipe_FOREIGN`) REFERENCES `equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

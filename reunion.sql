-- phpMyAdmin SQL Dump
-- version 4.2.8.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 16 Décembre 2015 à 17:55
-- Version du serveur :  10.0.13-MariaDB
-- Version de PHP :  5.5.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bddg1004`
--

-- --------------------------------------------------------

--
-- Structure de la table `reunion`
--

CREATE TABLE IF NOT EXISTS `reunion` (
`idReunion` int(11) NOT NULL,
  `nom` varchar(32) NOT NULL,
  `date` datetime NOT NULL,
  `ordre` varchar(255) DEFAULT NULL,
  `lieu` varchar(255) DEFAULT NULL,
  `idEquipe_FOREIGN` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `reunion`
--

INSERT INTO `reunion` (`idReunion`, `nom`, `date`, `ordre`, `lieu`, `idEquipe_FOREIGN`) VALUES
(1, 'rencontre_samedi', '2015-12-24 19:00:00', NULL, NULL, 4);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `reunion`
--
ALTER TABLE `reunion`
 ADD PRIMARY KEY (`idReunion`), ADD KEY `idEquipe_FOREIGN` (`idEquipe_FOREIGN`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `reunion`
--
ALTER TABLE `reunion`
MODIFY `idReunion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `reunion`
--
ALTER TABLE `reunion`
ADD CONSTRAINT `idEquipe_Foreign` FOREIGN KEY (`idEquipe_FOREIGN`) REFERENCES `equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php 
session_start();
?>
<!DOCTYPE>
<html>
  <head>
  	<meta charset="utf-8"/>
  	<title>Créer une équipe
  	</title>
  	<link rel="stylesheet" href="style.css"/>
  </head>
  <body>
  		<div class="logo"><a id="lienconnection" href="index.php"> KAGGLE</a>
			
				
		</div>
    	<?php
    	//if (!isset($_SESSION['pseudo'])){ //si l'utilisateur n'est pas connecté on lui affiche les boutons
		//echo '<div class="identification"> <a id="lienconnection" href="pagedeconnection.php">Se connecter/S\'inscrire</a></div>';
		//}
		//else{//sinon, il est connecté on lui propose donc, de se déconnecter
		echo '<a href="php/deconnexion.php"><img class="imgdeco" src="media/deco.png"></img></a>';
		//}
		?>
		
		<br/>
		<br/>
		<br/>
		<br/>
		<table>
			<tr>
				<?php
				//if (isset($_SESSION['pseudo'])){//si l'utilisateur est connecté on lui propose les 3 choix

					echo '<td id="menu"><a id="lienconnection" href="affichage_equipes.php">Afficher équipe</a></td>';			
					
					echo '<td id="menu"><a id="lienconnection" href="creer_equipe.php">Créer équipe</a></td>';
					if ($_SESSION['statutPers']>='2'){
						echo '<td id="menu"><a id="lienconnection" href="affichage_reunions.php">Afficher réunions</a></td>';
						}
						if ($_SESSION['statutPers']=='3'){
						echo '<td id="menu"><a id="lienconnection" href="espaceChefEquipe.php">Gestion equipe</a></td>';	
					echo '<td id="menu"><a id="lienconnection" href="affichage_compte_rendu.php">Compte rendu</a></td>'	;
						}
			//	}

				//else{//si l'utilisateur n'est pas connecté on affiche les différents messages

					//echo '<td id="menu"><a id="lienconnection" href="pagedeconnection.php">Vous n\'etes pas connecté</a></td>';
				//}
				?>


			</tr>
		</table>
		
		<br/>
		<div class="infomenu">
			<p class= "entetecreerequipe">CREER UNE EQUIPE </p>
			<form class= "formequipe" method="post" action="traitement_equipe.php">
 		<p>
       <label for="name">Nom de l'équipe </label>
       <input class="gererequipe" type="text" name="pseudo" id="name" placeholder="Ex : Chelsea"  maxlength="30" minlength="0" autofocus required/>
       </p>
       
       <p>
       <label for="nbrdemembr">Le nombre de membres : </label>
       <input class="gererequipe" type="number" name="nbrdemembr" id="nbrdemembr" placeholder="11" maxlength="12" minlength="0" required/>
       </p>
       
       <p>
	    <label for="typedequipe">Type d'équipe : </label>
       <input class="gererequipe" type="text" name="pass" id="typedequipe" placeholder="Ex : Foot" maxlength="12" minlength="0" required/>
       </p>
       
       <p>
	   <label for="idchefequipe"> Identifiant chef d'équipe : </label>
       <input class="gererequipe" type="number" name="Idchefequipe" id="idchefequipe" placeholder="Ex : 12345"  maxlength="12" minlength="0" required/>
       </p>
       
       <p>
	   <label for="mailchefequipe"> Mail chef d'équipe : </label>
       <input class="gererequipe" type="email" name="email" id="mailchefequipe" placeholder="Ex : xxxx@xx.xx"  maxlength="12" minlength="0" required/>
       </p>
       
       <p>
        <label for="tel"> portable : </label>
       <input class="gererequipe" type="tel" name="portable" id="tel" placeholder="Ex : 06xxxxxxxx"  maxlength="10" minlength="10" />
       </p>
       <br />


		<input class="gererequipe" type="submit" value="Envoyer"/>
    	<input class="gererequipe" type="reset" name="reset" id="reset" />
    	<a href="index.php" target="_blank"> <input type="button" value="Accueil"> </a>
    	<br/>

</form>
	

 <!-- blabla --> 
  
		
				<!--<?php// include("traitement_connexion.php"); ?>-->
			
		</div>
		<footer>
			<p>All Rights Reserved.</p>
		
			
		</footer>
  </body>
  </html>